PROJECT
    This project is a Ruby script which is able to group and sum a structured CSV file to show requested information.
    I group an array of loan objects by the tuple of Network, Product and Month summing the amount and count of loans.

USAGE
    All of the following command line inputs will initialize the script:

        - This assumes the file is called Loans.csv and is located in the root directory of the script
        - No benchmarking metrics will be displayed
            ruby ./main.rb

        - This will accept the csv at the path provided as a parameter
        - No benchmarking metrics will be displayed
            ruby ./main.rb "./Loans.csv"

        - This will accept the csv at the path provided as a parameter
        - Bench marking metrics will be displayed
            ruby ./main.rb "./Loans.csv" "attach_benchmark"

ASSUMPTIONS
    1) This is a showcase of ability, I have treated it as such.
    2) The grouping requested is 3 fold - Network AND Product AND Month, not by totals and counts grouped by EACH but rather ALL
    3) I do realize that the output would be identical to the input given the small dataset as such I have created a large copy of the original CSV in order to test the totals correctly and get a relevant benchmark
    4) The object orientation and encapsulation for this is a little bit overkill for a task of this nature unless a focus is reusability & readability
    5) As a result of the OOP this could be optimized to refactor looping and recursions
    6) If a non-oop version is requested I'd be more than happy to comply
    7) I did not sanitize the quotation marks in the string column as the client system my need this to interpret it as a string
    8) I included, considering this is a script that would reused in the real world - allowance of filepath as a parameter to prevents users from having to copy and paste files into the root path

SCALING CONSIDERATIONS
    I have benchmarked this solution with a 300 000 line CSV here are my findings:
        Ruby took ~8 second to open the file
        The processing grouping and summing took ~1.2 seconds
        The writing took ~6 split-seconds
    I have included the LoansLarge.csv file in the root directory for scalability testing.
    Conclusion
        It's very clear that opening the file is our bottle neck, we could refactor to pre-process and pre-screen records during the read time to better utilize the file memory pointer.
