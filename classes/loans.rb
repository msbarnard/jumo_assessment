class Loans
  require 'csv'

  attr_accessor :unprocessed
  attr_accessor :processed

  def initialize(path)
    self.unprocessed = read_file(path)
  end

  def process
    # This was done in an attempt to showcase my ability to deal with "complex" and nested datasets
    self.processed = self.unprocessed.group_by {|l|
      [l.network, l.product, Date::MONTHNAMES[l.date.month]]}.map {|titles, totals|
      [titles[0], titles[1], titles[2], totals.map {|total| total.amount}.reduce(:+), totals.length]}
  end

  def write
    CSV.open("./Output.csv", "wb") do |csv|
      csv << %w(Network Product Month Title Total Count)
      self.processed.each do |result|
        csv << result
      end
    end
  end

  private
  def read_file(path)
    _all = []
    CSV.foreach(path, headers: true) do |row|
      _all << Loan.new(row)
    end
    _all
  end


end