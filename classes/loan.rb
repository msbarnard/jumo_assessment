class Loan
  # This could be refactored as a hash but I prefer the readability of attribute readers in a class
  attr_accessor :msisdn, :network, :date, :product, :amount
  def initialize(row)
    self.msisdn = row[0]
    self.network = row[1]
    self.date = Date.strptime(row[2], "'%d-%b-%Y'")
    self.product = row[3]
    self.amount = row[4].to_f
  end
end
