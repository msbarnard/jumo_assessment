# Class references
require './classes/loan'
require './classes/loans'
if ARGV.length > 0
  if ARGV[1] == 'attach_benchmark'
    # Benchmarking:
    require 'benchmark'
    loans = Loans.new(ARGV[0])
    puts '=========================== BENCHMARKING ==========================='
    puts "=========================== #{loans.unprocessed.length} rows ==========================="
    Benchmark.bm do |bench|
      puts "\e[33m Data Structure: \e[0m"
      bench.report {
        loans = Loans.new(ARGV[0])
      }
      puts "\e[31m Processing:\e[0m"
      bench.report {
        loans.process
      }
      puts "\e[32m Writing: \e[0m"
      bench.report {
        loans.write
      }
    end
  else
    # Expected Implementation:
    loans = Loans.new(ARGV[0])
    loans.process
    loans.write
  end
else
  # Default Implementation:
  loans = Loans.new('./Loans.csv')
  loans.process
  loans.write
end

puts "Export Completed"
puts "Opening File..."
%x(open ./Output.csv)
